package com.example.pendingintentsandbox

import android.app.*
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.pendingintentsandbox.view.CreateIntentDialog
import com.example.pendingintentsandbox.view.CreatePendingIntentDialog
import com.example.pendingintentsandbox.view.Dialog
import com.example.pendingintentsandbox.view.HistoryStackAdapter
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.activity_main.*

abstract class BaseActivity : AppCompatActivity(), CreateIntentDialog.Listener, CreatePendingIntentDialog.Listener {

    companion object {
        private const val OPEN_ACTIVITY_REQUEST_CODE = 1
        private const val CREATE_PENDING_INTENT_REQUEST_CODE = 3
    }

    private val adapter: HistoryStackAdapter = HistoryStackAdapter()
    private var disposable: Disposable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setTitle(localClassName)

        createNotificationChannel()
        setOnClickListeners()

        main_textview_extras.text = "extras: ${intent.getStringExtra("extra")}"

        main_recyclerview.adapter = adapter
        main_recyclerview.layoutManager = LinearLayoutManager(this)

        registerActivityLifecycleCallbacks(object : Application.ActivityLifecycleCallbacks {
            override fun onActivityPaused(activity: Activity) {
            }

            override fun onActivityStarted(activity: Activity) {
            }

            override fun onActivityDestroyed(activity: Activity) {
                HistoryStackHolder.removeActivity(activity.hashCode(), taskId)
            }

            override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle) {
            }

            override fun onActivityStopped(activity: Activity) {
            }

            override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {
                HistoryStackHolder.addActivity(hashCode(), localClassName, taskId)
            }

            override fun onActivityResumed(activity: Activity) {
                HistoryStackHolder.setForegroundTask(taskId)
            }
        })
        HistoryStackHolder.addActivity(hashCode(), localClassName, taskId)

        disposable = HistoryStackHolder.listenStack()
            .subscribe {
                adapter.setData(it.values.toList())
            }
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable?.dispose()
    }

    private fun setOnClickListeners() {
        main_button_1.setOnClickListener {
            CreateIntentDialog(CREATE_PENDING_INTENT_REQUEST_CODE).show(supportFragmentManager, "ChooseIntentFlagDialog")
        }

        main_button_2.setOnClickListener {
            Dialog().show(supportFragmentManager, "dialog")
        }

        main_button_3.setOnClickListener {
            CreateIntentDialog(OPEN_ACTIVITY_REQUEST_CODE).show(supportFragmentManager, "ChooseIntentFlagDialog")
        }

        main_button_4.setOnClickListener {
            val intent = Intent(this, StandardActivity::class.java)
            startActivity(intent)
        }
    }

    private fun createNotification(pendingIntent: PendingIntent, title: String): Notification =
        NotificationCompat.Builder(this, "standard")
            .setContentIntent(pendingIntent)
            .setContentTitle(title)
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            .setSmallIcon(R.drawable.ic_launcher_foreground)
            .build()

    private fun createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = "standard"
            val descriptionText = "standard channel"
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel("standard", name, importance).apply {
                description = descriptionText
            }
            // Register the channel with the system
            val notificationManager: NotificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }

    override fun onFlagsChoosed(intent: Intent, requestCode: Int) {
        when (requestCode) {
            OPEN_ACTIVITY_REQUEST_CODE -> {
                startActivity(intent)
            }
            CREATE_PENDING_INTENT_REQUEST_CODE -> {
                CreatePendingIntentDialog(intent).show(supportFragmentManager, "CreatePendingIntentDialog")
            }
        }
    }

    override fun onPendingIntentCreated(pendingIntent: PendingIntent, notificationTitle: String) {
//                Log.d("qweqwe", "intent1==intent2 ${intent1.filterEquals(intent2)}")
//                Log.d("qweqwe", "pendingIntent1==pendingIntent2 ${pendingIntent1 == pendingIntent2}")
        val notif = createNotification(pendingIntent, notificationTitle)
        NotificationManagerCompat.from(this).notify(System.currentTimeMillis().toInt(), notif)
    }
}