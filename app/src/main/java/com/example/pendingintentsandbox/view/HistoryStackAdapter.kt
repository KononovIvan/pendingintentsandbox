package com.example.pendingintentsandbox.view

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.pendingintentsandbox.R
import com.example.pendingintentsandbox.entity.ActivityData
import com.example.pendingintentsandbox.entity.TaskData
import kotlinx.android.synthetic.main.item_activity_data.view.*
import kotlinx.android.synthetic.main.item_task_data.view.*

class HistoryStackAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    companion object {
        private val COLORS: List<Int> = listOf(
            Color.parseColor("#95FFF5"),
            Color.parseColor("#A6D38C"),
            Color.parseColor("#ECCA9A"),
            Color.parseColor("#FCBEBE"),
            Color.parseColor("#F3A1D1"),
            Color.parseColor("#C69ECC"),
            Color.parseColor("#9B8BFC"),
            Color.parseColor("#D5CDCD")
        )
    }

    private val backStack: MutableList<TaskData> = mutableListOf()

    fun setData(data: List<TaskData>) {
        backStack.clear()
        backStack.addAll(data.reversed())
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_task_data, parent, false))

    override fun getItemCount(): Int = backStack.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) =
        (holder as ViewHolder).bind(backStack[position])

    private class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        private val adapter = TaskAdapter()

        init {
            view.item_task_recyclerview.isNestedScrollingEnabled = false
            view.item_task_recyclerview.adapter = adapter
            view.item_task_recyclerview.layoutManager = LinearLayoutManager(view.context)
        }

        fun bind(data: TaskData) {
            itemView.item_task_textview_taskid.text = "taskId: ${data.id}"
            itemView.item_task_layout.setBackgroundColor(COLORS[data.id % COLORS.size])
            adapter.setData(data.activities.values.toList())
        }
    }

    private class TaskAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
        private val list: MutableList<ActivityData> = mutableListOf()

        fun setData(data: List<ActivityData>) {
            list.clear()
            list.addAll(data.reversed())
            notifyDataSetChanged()
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
            ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_activity_data, parent, false))

        override fun getItemCount(): Int = list.size

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) =
            (holder as ViewHolder).bind(list[position])

        private class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

            fun bind(data: ActivityData) {
                itemView.item_textview.text = "${data.name} (hash: ${data.hash})"
            }
        }
    }
}