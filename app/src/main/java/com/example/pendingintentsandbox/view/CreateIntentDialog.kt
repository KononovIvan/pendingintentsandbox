package com.example.pendingintentsandbox.view

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import androidx.fragment.app.DialogFragment
import com.example.pendingintentsandbox.MainActivity
import com.example.pendingintentsandbox.R
import com.example.pendingintentsandbox.SecondActivity
import kotlinx.android.synthetic.main.dialog_fragment_choose_intent_flags.*

class CreateIntentDialog(val requestCode: Int) : DialogFragment() {

    private val flags = linkedMapOf<Int, String>(
        Pair(Intent.FLAG_ACTIVITY_SINGLE_TOP, "FLAG_ACTIVITY_SINGLE_TOP"),
        Pair(Intent.FLAG_ACTIVITY_CLEAR_TOP, "FLAG_ACTIVITY_CLEAR_TOP"),
        Pair(Intent.FLAG_ACTIVITY_NEW_TASK, "FLAG_ACTIVITY_NEW_TASK"),
        Pair(Intent.FLAG_ACTIVITY_CLEAR_TASK, "FLAG_ACTIVITY_CLEAR_TASK"),
        Pair(Intent.FLAG_ACTIVITY_NO_HISTORY, "FLAG_ACTIVITY_NO_HISTORY"),
        Pair(Intent.FLAG_ACTIVITY_MULTIPLE_TASK, "FLAG_ACTIVITY_MULTIPLE_TASK"),
        Pair(Intent.FLAG_ACTIVITY_FORWARD_RESULT, "FLAG_ACTIVITY_FORWARD_RESULT"),
        Pair(Intent.FLAG_ACTIVITY_PREVIOUS_IS_TOP, "FLAG_ACTIVITY_PREVIOUS_IS_TOP"),
        Pair(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS, "FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS"),
        Pair(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT, "FLAG_ACTIVITY_BROUGHT_TO_FRONT"),
        Pair(Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED, "FLAG_ACTIVITY_RESET_TASK_IF_NEEDED"),
        Pair(Intent.FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY, "FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY"),
        Pair(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET, "FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET"),
        Pair(Intent.FLAG_ACTIVITY_NEW_DOCUMENT, "FLAG_ACTIVITY_NEW_DOCUMENT"),
        Pair(Intent.FLAG_ACTIVITY_NO_USER_ACTION, "FLAG_ACTIVITY_NO_USER_ACTION"),
        Pair(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT, "FLAG_ACTIVITY_REORDER_TO_FRONT"),
        Pair(Intent.FLAG_ACTIVITY_NO_ANIMATION, "FLAG_ACTIVITY_NO_ANIMATION"),
        Pair(Intent.FLAG_ACTIVITY_TASK_ON_HOME, "FLAG_ACTIVITY_TASK_ON_HOME"),
        Pair(Intent.FLAG_ACTIVITY_RETAIN_IN_RECENTS, "FLAG_ACTIVITY_RETAIN_IN_RECENTS"),
        Pair(Intent.FLAG_ACTIVITY_LAUNCH_ADJACENT, "FLAG_ACTIVITY_LAUNCH_ADJACENT"),
        Pair(Intent.FLAG_ACTIVITY_MATCH_EXTERNAL, "FLAG_ACTIVITY_MATCH_EXTERNAL")
    )

    private val checkBoxes: MutableList<CheckBox> = mutableListOf()
    private var listener: Listener? = null

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        if (context is Listener) {
            listener = context
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        checkBoxes.clear()
        flags.forEach {
            val checkBox = CheckBox(context)
            checkBox.text = it.value
            checkBox.tag = it.key
            checkBoxes.add(checkBox)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.dialog_fragment_choose_intent_flags, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        dialog_button_cancel.setOnClickListener { dismiss() }
        dialog_button_ok.setOnClickListener {
            var _flags: Int = 0
            var _class: Class<*> = MainActivity::class.java

            checkBoxes.forEach {
                if (it.isChecked) {
                    _flags = _flags or (it.tag as Int)
                }
            }
            if (dialog_radiobutton_2.isChecked) _class = SecondActivity::class.java

            val intent = Intent(context, _class)
            intent.flags = _flags

            if (!dialog_edittext_extra.text.isNullOrBlank()) {
                intent.putExtra("extra", dialog_edittext_extra.text.toString())
            }

            listener?.onFlagsChoosed(intent, requestCode)
            dismiss()
        }

        checkBoxes.forEach {
            dialog_checkbox_container.addView(it)
        }
    }

    interface Listener {
        fun onFlagsChoosed(intent: Intent, requestCode: Int)
    }
}