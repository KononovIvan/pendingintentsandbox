package com.example.pendingintentsandbox.view

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import androidx.fragment.app.DialogFragment
import com.example.pendingintentsandbox.R
import kotlinx.android.synthetic.main.dialog_fragment_create_pendingintent.*

class CreatePendingIntentDialog(val intent: Intent) : DialogFragment() {

    private val flags = linkedMapOf<Int, String>(
        Pair(PendingIntent.FLAG_ONE_SHOT, "FLAG_ONE_SHOT"),
        Pair(PendingIntent.FLAG_NO_CREATE, "FLAG_NO_CREATE"),
        Pair(PendingIntent.FLAG_CANCEL_CURRENT, "FLAG_CANCEL_CURRENT"),
        Pair(PendingIntent.FLAG_UPDATE_CURRENT, "FLAG_UPDATE_CURRENT"),
        Pair(PendingIntent.FLAG_IMMUTABLE, "FLAG_IMMUTABLE")
    )

    private val radioButtons: MutableList<RadioButton> = mutableListOf()
    private var listener: Listener? = null

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        if (context is Listener) {
            listener = context
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        radioButtons.clear()
        flags.forEach {
            val radioButton = RadioButton(context)
            radioButton.text = it.value
            radioButton.tag = it.key
            radioButtons.add(radioButton)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.dialog_fragment_create_pendingintent, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        dialog_button_cancel.setOnClickListener { dismiss() }
        dialog_button_ok.setOnClickListener {
            var _flag = 0
            radioButtons.forEach {
                if (it.isChecked) {
                    _flag = (it.tag as Int)
                    return@forEach
                }
            }

            var requestCode = 0
            var notificationTitle = "notification title"
            if (!dialog_edittext_request_code.text.isNullOrBlank()) {
                requestCode = dialog_edittext_request_code.text.toString().toInt()
            }
            if (!dialog_edittext_name.text.isNullOrBlank()) {
                notificationTitle = dialog_edittext_name.text.toString()
            }

            val pintent = PendingIntent.getActivity(context, requestCode, intent, _flag)

            listener?.onPendingIntentCreated(pintent, notificationTitle)
            dismiss()
        }

        radioButtons.forEach {
            dialog_radiogroup.addView(it)
            if (it.tag.equals(PendingIntent.FLAG_UPDATE_CURRENT)) {
                it.isChecked = true
            }
        }
    }

    interface Listener {
        fun onPendingIntentCreated(pendingIntent: PendingIntent, notificationTitle: String)
    }
}