package com.example.pendingintentsandbox.entity

data class TaskData(
    val id: Int,
    val activities: LinkedHashMap<Int, ActivityData>
)