package com.example.pendingintentsandbox.entity

data class ActivityData(
    val hash: Int,
    val name: String
)