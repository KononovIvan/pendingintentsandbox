package com.example.pendingintentsandbox

import com.example.pendingintentsandbox.entity.ActivityData
import com.example.pendingintentsandbox.entity.TaskData
import io.reactivex.subjects.BehaviorSubject

object HistoryStackHolder {

    private var counter: Int = 0
    private val stack: LinkedHashMap<Int, TaskData> = linkedMapOf()
    private val subject: BehaviorSubject<LinkedHashMap<Int, TaskData>> = BehaviorSubject.createDefault(stack)

    fun removeActivity(id: Int, taskId: Int) {
        val res1 = stack[taskId]?.activities?.remove(id)
        var res2: TaskData? = null
        if (stack[taskId]?.activities.isNullOrEmpty()) {
            res2 = stack.remove(taskId)
        }
        if (res1 != null || res2 != null) subject.onNext(stack)
    }

    fun addActivity(id: Int, name: String, taskId: Int) {
        stack[taskId] = (stack[taskId] ?: TaskData(taskId, linkedMapOf())).apply {
            activities[id] = ActivityData(id, "#${counter++}: $name")
        }
        subject.onNext(stack)
    }

    fun setForegroundTask(taskId: Int) {
        if (stack.keys.last() != taskId) {
            val task = stack[taskId]?.copy()
            if (task != null) {
                stack.remove(taskId)
                stack[taskId] = task
                subject.onNext(stack)
            }
        }
    }

    fun listenStack(): BehaviorSubject<LinkedHashMap<Int, TaskData>> = subject
}